# Some notes on the Summary

This summary contains the topics covered in the lectures from "Reliable and Interpretable Artificial Intelligence", taught at ETH in 2020 by Martin Vechev.
It is not intended to serve as learning material, but rather as a support when solving exercises and exams.
It fulfills the requirements for a summary you can bring along for the exam, which are 11pt minimum font size and 2 pages.
Note that the summary might not be super complete, since I omitted a bit of stuff, which I felt already very comfortable with (such as definitions of CNNs).
And of course I cannot guarantee that there are no mistakes.
So if you require something additional, feel free to fork the repo and create your own version (and maybe again upload it for others to use).
There is still some space left for some more stuff and you can even compress everything more by reducing the page margins and allowing 5 instead of 4 columns.

The format is "stolen" from Marilou Beyeler's [Machine Learning cheat sheet](https://github.com/mariloubylr/eth-summaries). Credits (as well as complaints :)) go to her!

Have fun with your preparations for this super interesting topic!
